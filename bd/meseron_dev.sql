-- MySQL dump 10.13  Distrib 5.6.21, for osx10.6 (x86_64)
--
-- Host: localhost    Database: meseron_dev
-- ------------------------------------------------------
-- Server version	5.6.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin_users`
--

DROP TABLE IF EXISTS `admin_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `admin_users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin_users`
--

LOCK TABLES `admin_users` WRITE;
/*!40000 ALTER TABLE `admin_users` DISABLE KEYS */;
INSERT INTO `admin_users` VALUES (1,'admin','admin','0000-00-00 00:00:00',NULL);
/*!40000 ALTER TABLE `admin_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `restaurant_tables`
--

DROP TABLE IF EXISTS `restaurant_tables`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `restaurant_tables` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_restaurant_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `available` tinyint(1) NOT NULL DEFAULT '1',
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_tbl_restaurant_tables_tbl_restaurants_idx` (`fk_restaurant_id`),
  CONSTRAINT `fk_tbl_restaurant_tables_tbl_restaurants` FOREIGN KEY (`fk_restaurant_id`) REFERENCES `restaurants` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `restaurant_tables`
--

LOCK TABLES `restaurant_tables` WRITE;
/*!40000 ALTER TABLE `restaurant_tables` DISABLE KEYS */;
INSERT INTO `restaurant_tables` VALUES (7,1,'mesa 1 r1',1,'0000-00-00 00:00:00','2016-01-30 15:37:13'),(8,2,'mesa 1 r2',1,'0000-00-00 00:00:00',NULL),(9,1,'mesa 2 r1',1,'0000-00-00 00:00:00','2016-01-24 19:21:52'),(10,1,'mesa 3 r1',1,'0000-00-00 00:00:00',NULL),(11,2,'mesa 2 r2',1,'0000-00-00 00:00:00',NULL);
/*!40000 ALTER TABLE `restaurant_tables` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `restaurants`
--

DROP TABLE IF EXISTS `restaurants`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `restaurants` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `logo` varchar(255) NOT NULL,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `restaurants`
--

LOCK TABLES `restaurants` WRITE;
/*!40000 ALTER TABLE `restaurants` DISABLE KEYS */;
INSERT INTO `restaurants` VALUES (1,'Restaurante 1','res1','res1','aa69c-california-t-thumb.jpg','0000-00-00 00:00:00',NULL),(2,'Restaurante 2','res2','res2','dd79a-ferrari_california1.jpg','0000-00-00 00:00:00',NULL);
/*!40000 ALTER TABLE `restaurants` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service_history`
--

DROP TABLE IF EXISTS `service_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service_history` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_service_id` int(11) NOT NULL,
  `action` varchar(255) NOT NULL,
  `data` varchar(255) DEFAULT NULL,
  `createdAt` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updatedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_service_history_services1_idx` (`fk_service_id`),
  CONSTRAINT `fk_service_history_services1` FOREIGN KEY (`fk_service_id`) REFERENCES `services` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_history`
--

LOCK TABLES `service_history` WRITE;
/*!40000 ALTER TABLE `service_history` DISABLE KEYS */;
/*!40000 ALTER TABLE `service_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `service_status`
--

DROP TABLE IF EXISTS `service_status`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `service_status` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `status` varchar(255) NOT NULL,
  `action` varchar(255) NOT NULL,
  `status_message` text,
  `createdAt` datetime DEFAULT NULL,
  `updatedAt` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `service_status`
--

LOCK TABLES `service_status` WRITE;
/*!40000 ALTER TABLE `service_status` DISABLE KEYS */;
INSERT INTO `service_status` VALUES (1,'menu','entregado','En breve seras atendido...',NULL,NULL),(2,'cliente leyendo menu','',NULL,NULL,NULL),(3,'tomar orden','tomada','Estas listo para ordernar. En breve seras atendido',NULL,NULL),(4,'pedido','entregado','Has ordenado, En breve llevaremos tu orden',NULL,NULL),(5,'solicitud de cliente por otra orden o cuenta','',NULL,NULL,NULL),(6,'cuenta','enviar','Tu mesero esta generando la información de la cuenta, en breve te será enviada',NULL,NULL),(7,'Metodo de pago seleccionado por el cliente','',NULL,NULL,NULL),(8,'Esperando por datafono / efectivo','Pagada','En breve tu mesero ira para recibir el pago',NULL,NULL),(9,'Sevicio terminado','',NULL,NULL,NULL);
/*!40000 ALTER TABLE `service_status` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `services`
--

DROP TABLE IF EXISTS `services`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_waiter_id` int(11) NOT NULL,
  `fk_user_id` int(11) NOT NULL,
  `fk_table_id` int(11) NOT NULL,
  `fk_status_id` int(11) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `data` text,
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_service_waiter1_idx` (`fk_waiter_id`),
  KEY `fk_service_users1_idx` (`fk_user_id`),
  KEY `fk_services_restaurant_tables1_idx` (`fk_table_id`),
  KEY `fk_services_service_status1_idx` (`fk_status_id`),
  CONSTRAINT `fk_service_users1` FOREIGN KEY (`fk_user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_service_waiter1` FOREIGN KEY (`fk_waiter_id`) REFERENCES `waiters` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `fk_services_restaurant_tables1` FOREIGN KEY (`fk_table_id`) REFERENCES `restaurant_tables` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_services_service_status1` FOREIGN KEY (`fk_status_id`) REFERENCES `service_status` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `services`
--

LOCK TABLES `services` WRITE;
/*!40000 ALTER TABLE `services` DISABLE KEYS */;
INSERT INTO `services` VALUES (6,1,6,7,1,0,NULL,'2016-01-26 08:31:32','2016-01-26 08:41:43'),(7,1,7,7,1,0,NULL,'2016-01-26 08:42:03','2016-01-26 08:43:09'),(8,1,8,7,1,0,NULL,'2016-01-26 08:43:18','2016-01-26 08:50:02'),(9,1,9,7,1,0,NULL,'2016-01-26 08:50:12','2016-01-26 08:54:19'),(10,1,10,7,1,0,NULL,'2016-01-26 08:54:32','2016-01-26 08:55:22'),(11,1,11,7,1,0,NULL,'2016-01-26 08:55:31','2016-01-26 08:59:35'),(12,1,12,7,1,0,NULL,'2016-01-26 08:59:45','2016-01-26 09:01:25'),(13,1,13,7,1,0,NULL,'2016-01-26 09:01:35','2016-01-26 09:02:26'),(14,1,14,7,1,0,NULL,'2016-01-26 09:02:35','2016-01-26 09:04:19'),(15,1,15,7,1,0,NULL,'2016-01-26 09:04:28','2016-01-26 09:28:34'),(16,1,16,7,1,0,NULL,'2016-01-26 09:28:52','2016-01-26 09:29:43'),(17,1,17,7,1,0,NULL,'2016-01-26 09:29:57','2016-01-26 09:31:03'),(18,1,18,7,1,0,NULL,'2016-01-26 09:31:15','2016-01-26 09:33:13'),(19,1,19,7,1,0,NULL,'2016-01-26 09:33:23','2016-01-26 09:50:06'),(20,1,20,7,1,0,NULL,'2016-01-26 09:50:21','2016-01-26 09:51:38'),(21,1,21,7,1,0,NULL,'2016-01-26 09:51:50','2016-01-26 10:14:38'),(22,1,23,7,1,0,NULL,'2016-01-26 10:16:36','2016-01-26 10:18:00'),(23,1,24,7,1,0,NULL,'2016-01-26 10:19:14','2016-01-26 10:58:41'),(24,1,25,7,2,0,NULL,'2016-01-26 10:58:51','2016-01-26 10:59:30'),(25,1,27,7,2,0,NULL,'2016-01-26 11:00:30','2016-01-26 11:02:10'),(26,1,28,7,1,0,NULL,'2016-01-26 11:02:19','2016-01-26 11:02:32'),(27,1,29,7,1,0,NULL,'2016-01-26 11:04:42','2016-01-26 11:05:15'),(28,1,30,7,1,0,NULL,'2016-01-26 11:08:26','2016-01-26 11:16:28'),(29,1,31,7,1,0,NULL,'2016-01-26 11:16:40','2016-01-26 11:17:31'),(30,1,32,7,2,0,NULL,'2016-01-26 11:17:40','2016-01-26 11:52:24'),(31,1,33,7,2,0,NULL,'2016-01-26 11:53:10','2016-01-26 11:57:57'),(32,1,34,7,2,0,NULL,'2016-01-26 11:58:19','2016-01-26 11:58:56'),(33,1,35,7,3,0,NULL,'2016-01-26 11:59:06','2016-01-26 11:59:30'),(34,1,36,7,1,0,NULL,'2016-01-26 14:46:12','2016-01-26 14:49:44'),(35,1,37,7,3,0,NULL,'2016-01-26 14:49:55','2016-01-26 15:05:27'),(36,1,38,7,3,0,NULL,'2016-01-26 15:05:36','2016-01-26 15:05:59'),(37,1,39,7,3,0,NULL,'2016-01-26 15:06:11','2016-01-26 15:07:38'),(38,1,40,7,3,0,NULL,'2016-01-26 15:07:45','2016-01-26 15:12:19'),(39,1,41,7,4,0,NULL,'2016-01-26 15:12:55','2016-01-26 15:14:05'),(40,1,42,7,4,0,NULL,'2016-01-26 15:14:17','2016-01-26 15:32:41'),(41,1,43,7,4,0,NULL,'2016-01-26 15:32:55','2016-01-26 15:33:26'),(42,1,44,7,5,0,NULL,'2016-01-26 15:33:35','2016-01-26 15:47:11'),(43,1,45,7,1,0,NULL,'2016-01-26 15:47:40','2016-01-26 15:47:56'),(44,1,46,7,5,0,NULL,'2016-01-26 15:48:06','2016-01-26 16:01:02'),(45,1,47,7,5,0,NULL,'2016-01-26 16:01:14','2016-01-28 08:20:13'),(46,1,48,7,1,0,NULL,'2016-01-28 08:20:26','2016-01-28 08:21:06'),(47,1,49,7,2,0,NULL,'2016-01-28 08:23:42','2016-01-28 08:24:09'),(48,1,50,7,5,0,NULL,'2016-01-28 08:25:38','2016-01-28 08:45:20'),(49,1,51,7,6,0,NULL,'2016-01-28 08:45:29','2016-01-28 09:08:59'),(50,1,52,7,6,0,NULL,'2016-01-28 09:09:06','2016-01-28 09:09:37'),(51,1,53,7,6,0,NULL,'2016-01-28 09:09:46','2016-01-28 10:06:38'),(52,1,54,7,6,0,NULL,'2016-01-28 10:06:48','2016-01-28 10:58:40'),(53,1,55,7,5,0,NULL,'2016-01-28 10:59:35','2016-01-28 11:29:47'),(54,1,56,7,7,0,NULL,'2016-01-28 11:30:42','2016-01-28 11:37:34'),(55,1,57,7,7,0,NULL,'2016-01-28 11:37:39','2016-01-28 11:48:58'),(56,1,58,7,7,0,NULL,'2016-01-28 11:50:01','2016-01-28 11:54:15'),(57,1,59,7,7,0,NULL,'2016-01-28 11:54:25','2016-01-28 11:55:59'),(58,1,60,7,7,0,NULL,'2016-01-28 11:56:08','2016-01-28 11:57:20'),(59,1,61,7,7,0,NULL,'2016-01-28 11:57:45','2016-01-28 12:00:47'),(60,1,62,7,7,0,'{\"checkValue\":15000,\"serviceValue\":1500}','2016-01-28 12:01:23','2016-01-28 14:36:35'),(61,1,63,7,7,0,'{\"checkValue\":50000,\"serviceValue\":5000}','2016-01-28 14:36:47','2016-01-28 14:42:17'),(62,1,64,7,7,0,'{\"checkValue\":50000,\"serviceValue\":5000}','2016-01-28 14:42:34','2016-01-28 14:45:49'),(63,1,65,7,7,0,'{\"checkValue\":50000,\"serviceValue\":5000}','2016-01-28 14:49:09','2016-01-28 15:18:08'),(64,1,66,7,7,0,'{\"checkValue\":68000,\"serviceValue\":6800}','2016-01-28 15:18:18','2016-01-28 16:03:55'),(65,1,67,7,7,0,'{\"checkValue\":55000,\"serviceValue\":5000}','2016-01-28 16:04:06','2016-01-28 16:12:49'),(66,1,68,7,5,0,NULL,'2016-01-29 10:21:54','2016-01-29 10:22:09'),(67,1,69,7,3,0,'{\"checkValue\":55000,\"serviceValue\":14000}','2016-01-29 10:22:19','2016-01-29 10:26:49'),(68,1,70,7,6,0,NULL,'2016-01-29 10:27:00','2016-01-29 10:29:05'),(69,1,71,7,7,0,'{\"checkValue\":50000,\"serviceValue\":55000}','2016-01-29 10:29:17','2016-01-29 11:04:20'),(70,1,72,7,7,0,'{\"checkValue\":70000,\"serviceValue\":7000}','2016-01-29 11:04:34','2016-01-29 11:13:50'),(71,1,73,7,5,0,NULL,'2016-01-29 11:14:26','2016-01-29 11:14:50'),(72,1,74,7,8,0,'{\"payType\":\"dataphone\",\"serviceValue\":5500}','2016-01-29 11:14:58','2016-01-29 11:17:41'),(73,1,75,7,8,0,'{\"payType\":\"cash\",\"serviceValue\":4500}','2016-01-29 11:18:13','2016-01-29 11:52:17'),(74,1,76,7,8,0,'{\"payType\":\"dataphone\",\"serviceValue\":3500}','2016-01-29 11:52:27','2016-01-29 11:53:47'),(75,1,77,7,8,0,'{\"payType\":\"dataphone\",\"serviceValue\":222}','2016-01-29 11:53:55','2016-01-29 11:55:02'),(76,1,78,7,8,0,'{\"payType\":\"cash\",\"serviceValue\":15000}','2016-01-30 12:37:03','2016-01-30 13:18:11'),(77,1,79,7,8,0,'{\"payType\":\"cash\",\"serviceValue\":15000}','2016-01-30 13:18:38','2016-01-30 13:20:40'),(78,1,80,7,8,0,'{\"payType\":\"cash\",\"serviceValue\":14000}','2016-01-30 13:20:48','2016-01-30 13:24:48'),(79,1,81,7,8,0,'{\"payType\":\"cash\",\"serviceValue\":12000}','2016-01-30 13:25:02','2016-01-30 13:26:02'),(80,1,82,7,8,0,'{\"payType\":\"cash\",\"serviceValue\":1}','2016-01-30 13:26:20','2016-01-30 13:28:56'),(81,1,83,7,9,0,'{\"payType\":\"cash\",\"serviceValue\":14000}','2016-01-30 13:29:06','2016-01-30 13:31:23'),(82,1,84,7,9,0,'{\"payType\":\"cash\",\"serviceValue\":111}','2016-01-30 13:34:20','2016-01-30 13:34:36'),(83,1,85,7,9,0,'{\"payType\":\"cash\",\"serviceValue\":12}','2016-01-30 13:37:47','2016-01-30 13:38:03'),(84,1,86,7,9,0,'{\"payType\":\"cash\",\"serviceValue\":1}','2016-01-30 13:41:26','2016-01-30 13:41:41'),(85,1,87,7,9,0,'{\"payType\":\"cash\",\"serviceValue\":1}','2016-01-30 13:42:16','2016-01-30 13:42:39'),(86,1,88,7,9,0,'{\"payType\":\"cash\",\"serviceValue\":12}','2016-01-30 13:51:33','2016-01-30 13:51:49'),(87,1,89,7,9,0,'{\"payType\":\"cash\",\"serviceValue\":12}','2016-01-30 14:01:57','2016-01-30 14:02:14'),(88,1,90,7,9,0,'{\"payType\":\"dataphone\",\"serviceValue\":12}','2016-01-30 14:06:05','2016-01-30 14:06:25'),(89,1,91,7,9,0,'{\"payType\":\"cash\",\"serviceValue\":12}','2016-01-30 14:17:32','2016-01-30 14:17:53'),(90,1,92,7,9,0,'{\"payType\":\"cash\",\"serviceValue\":12}','2016-01-30 14:19:12','2016-01-30 14:19:29'),(91,1,93,7,9,0,'{\"payType\":\"dataphone\",\"serviceValue\":122}','2016-01-30 14:20:14','2016-01-30 14:20:29'),(92,1,94,7,9,0,'{\"payType\":\"dataphone\",\"serviceValue\":1}','2016-01-30 14:22:00','2016-01-30 14:22:17'),(93,1,95,7,9,0,'{\"payType\":\"dataphone\",\"serviceValue\":1}','2016-01-30 14:23:46','2016-01-30 14:24:01'),(94,1,96,7,9,0,'{\"payType\":\"dataphone\",\"serviceValue\":1}','2016-01-30 14:25:16','2016-01-30 14:25:31'),(95,1,97,7,9,0,'{\"payType\":\"cash\",\"serviceValue\":112}','2016-01-30 15:12:18','2016-01-30 15:12:36'),(96,1,98,7,9,0,'{\"payType\":\"dataphone\",\"serviceValue\":1}','2016-01-30 15:17:53','2016-01-30 15:18:07'),(97,1,99,7,9,0,'{\"payType\":\"cash\",\"serviceValue\":122}','2016-01-30 15:19:32','2016-01-30 15:19:47'),(98,1,100,7,9,0,'{\"payType\":\"dataphone\",\"serviceValue\":2}','2016-01-30 15:32:08','2016-01-30 15:32:27'),(99,1,91,7,9,0,'{\"payType\":\"cash\",\"serviceValue\":122}','2016-01-30 15:34:24','2016-01-30 15:34:49'),(100,1,91,7,9,0,'{\"payType\":\"cash\",\"serviceValue\":12}','2016-01-30 15:36:56','2016-01-30 15:37:13');
/*!40000 ALTER TABLE `services` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_restaurant_id` int(11) NOT NULL,
  `fk_user_title_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `subscribed` tinyint(1) NOT NULL DEFAULT '0',
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  KEY `fk_users_users_title1_idx` (`fk_user_title_id`),
  KEY `fk_users_restaurants1_idx` (`fk_restaurant_id`),
  CONSTRAINT `fk_users_restaurants1` FOREIGN KEY (`fk_restaurant_id`) REFERENCES `restaurants` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_users_users_title1` FOREIGN KEY (`fk_user_title_id`) REFERENCES `users_title` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (6,1,1,'test',NULL,0,'2016-01-26 08:31:32','2016-01-26 08:31:32'),(7,1,1,'test',NULL,0,'2016-01-26 08:42:03','2016-01-26 08:42:03'),(8,1,1,'test',NULL,0,'2016-01-26 08:43:18','2016-01-26 08:43:18'),(9,1,1,'test',NULL,0,'2016-01-26 08:50:12','2016-01-26 08:50:12'),(10,1,1,'test',NULL,0,'2016-01-26 08:54:31','2016-01-26 08:54:31'),(11,1,1,'test',NULL,0,'2016-01-26 08:55:31','2016-01-26 08:55:31'),(12,1,1,'fdsf',NULL,0,'2016-01-26 08:59:45','2016-01-26 08:59:45'),(13,1,1,'fdsf',NULL,0,'2016-01-26 09:01:35','2016-01-26 09:01:35'),(14,1,1,'dsf',NULL,0,'2016-01-26 09:02:34','2016-01-26 09:02:34'),(15,1,1,'fsd',NULL,0,'2016-01-26 09:04:28','2016-01-26 09:04:28'),(16,1,1,'dfsf',NULL,0,'2016-01-26 09:28:52','2016-01-26 09:28:52'),(17,1,1,'test',NULL,0,'2016-01-26 09:29:57','2016-01-26 09:29:57'),(18,1,1,'test',NULL,0,'2016-01-26 09:31:15','2016-01-26 09:31:15'),(19,1,1,'test',NULL,0,'2016-01-26 09:33:23','2016-01-26 09:33:23'),(20,1,1,'test',NULL,0,'2016-01-26 09:50:21','2016-01-26 09:50:21'),(21,1,1,'qrx',NULL,0,'2016-01-26 09:51:50','2016-01-26 09:51:50'),(22,1,1,'test',NULL,0,'2016-01-26 10:14:47','2016-01-26 10:14:47'),(23,1,1,'fsdf',NULL,0,'2016-01-26 10:16:36','2016-01-26 10:16:36'),(24,1,1,'TEST',NULL,0,'2016-01-26 10:19:14','2016-01-26 10:19:14'),(25,1,1,'fdsf',NULL,0,'2016-01-26 10:58:51','2016-01-26 10:58:51'),(26,1,1,'fdsf',NULL,0,'2016-01-26 10:59:38','2016-01-26 10:59:38'),(27,1,1,'test',NULL,0,'2016-01-26 11:00:30','2016-01-26 11:00:30'),(28,1,1,'fdsf',NULL,0,'2016-01-26 11:02:19','2016-01-26 11:02:19'),(29,1,1,'test',NULL,0,'2016-01-26 11:04:42','2016-01-26 11:04:42'),(30,1,1,'test',NULL,0,'2016-01-26 11:08:26','2016-01-26 11:08:26'),(31,1,1,'test',NULL,0,'2016-01-26 11:16:40','2016-01-26 11:16:40'),(32,1,1,'test',NULL,0,'2016-01-26 11:17:40','2016-01-26 11:17:40'),(33,1,1,'test',NULL,0,'2016-01-26 11:53:10','2016-01-26 11:53:10'),(34,1,1,'fdsf',NULL,0,'2016-01-26 11:58:19','2016-01-26 11:58:19'),(35,1,1,'tester',NULL,0,'2016-01-26 11:59:06','2016-01-26 11:59:06'),(36,1,1,'FFF',NULL,0,'2016-01-26 14:46:12','2016-01-26 14:46:12'),(37,1,1,'test',NULL,0,'2016-01-26 14:49:55','2016-01-26 14:49:55'),(38,1,1,'test',NULL,0,'2016-01-26 15:05:36','2016-01-26 15:05:36'),(39,1,1,'test',NULL,0,'2016-01-26 15:06:11','2016-01-26 15:06:11'),(40,1,1,'test',NULL,0,'2016-01-26 15:07:45','2016-01-26 15:07:45'),(41,1,1,'test',NULL,0,'2016-01-26 15:12:54','2016-01-26 15:12:54'),(42,1,1,'test',NULL,0,'2016-01-26 15:14:17','2016-01-26 15:14:17'),(43,1,1,'test',NULL,0,'2016-01-26 15:32:55','2016-01-26 15:32:55'),(44,1,1,'test',NULL,0,'2016-01-26 15:33:35','2016-01-26 15:33:35'),(45,1,1,'tes',NULL,0,'2016-01-26 15:47:40','2016-01-26 15:47:40'),(46,1,1,'test',NULL,0,'2016-01-26 15:48:06','2016-01-26 15:48:06'),(47,1,1,'test',NULL,0,'2016-01-26 16:01:14','2016-01-26 16:01:14'),(48,1,1,'test',NULL,0,'2016-01-28 08:20:25','2016-01-28 08:20:25'),(49,1,1,'test',NULL,0,'2016-01-28 08:23:41','2016-01-28 08:23:41'),(50,1,1,'test',NULL,0,'2016-01-28 08:25:38','2016-01-28 08:25:38'),(51,1,1,'test',NULL,0,'2016-01-28 08:45:29','2016-01-28 08:45:29'),(52,1,1,'tes',NULL,0,'2016-01-28 09:09:06','2016-01-28 09:09:06'),(53,1,1,'ts',NULL,0,'2016-01-28 09:09:46','2016-01-28 09:09:46'),(54,1,1,'test',NULL,0,'2016-01-28 10:06:48','2016-01-28 10:06:48'),(55,1,1,'test',NULL,0,'2016-01-28 10:59:35','2016-01-28 10:59:35'),(56,1,1,'test',NULL,0,'2016-01-28 11:30:42','2016-01-28 11:30:42'),(57,1,1,'test',NULL,0,'2016-01-28 11:37:39','2016-01-28 11:37:39'),(58,1,1,'test',NULL,0,'2016-01-28 11:50:01','2016-01-28 11:50:01'),(59,1,1,'tes',NULL,0,'2016-01-28 11:54:25','2016-01-28 11:54:25'),(60,1,1,'test',NULL,0,'2016-01-28 11:56:08','2016-01-28 11:56:08'),(61,1,1,'test',NULL,0,'2016-01-28 11:57:45','2016-01-28 11:57:45'),(62,1,1,'test',NULL,0,'2016-01-28 12:01:23','2016-01-28 12:01:23'),(63,1,1,'test',NULL,0,'2016-01-28 14:36:47','2016-01-28 14:36:47'),(64,1,1,'test',NULL,0,'2016-01-28 14:42:34','2016-01-28 14:42:34'),(65,1,1,'test',NULL,0,'2016-01-28 14:49:09','2016-01-28 14:49:09'),(66,1,1,'test',NULL,0,'2016-01-28 15:18:18','2016-01-28 15:18:18'),(67,1,1,'test',NULL,0,'2016-01-28 16:04:06','2016-01-28 16:04:06'),(68,1,1,'test',NULL,0,'2016-01-29 10:21:53','2016-01-29 10:21:53'),(69,1,1,'test',NULL,0,'2016-01-29 10:22:19','2016-01-29 10:22:19'),(70,1,1,'test',NULL,0,'2016-01-29 10:27:00','2016-01-29 10:27:00'),(71,1,1,'test',NULL,0,'2016-01-29 10:29:17','2016-01-29 10:29:17'),(72,1,1,'test',NULL,0,'2016-01-29 11:04:34','2016-01-29 11:04:34'),(73,1,1,'test',NULL,0,'2016-01-29 11:14:26','2016-01-29 11:14:26'),(74,1,1,'test',NULL,0,'2016-01-29 11:14:58','2016-01-29 11:14:58'),(75,1,1,'test',NULL,0,'2016-01-29 11:18:13','2016-01-29 11:18:13'),(76,1,1,'fdsf',NULL,0,'2016-01-29 11:52:27','2016-01-29 11:52:27'),(77,1,1,'test',NULL,0,'2016-01-29 11:53:55','2016-01-29 11:53:55'),(78,1,1,'test',NULL,0,'2016-01-30 12:37:03','2016-01-30 12:37:03'),(79,1,1,'test',NULL,0,'2016-01-30 13:18:38','2016-01-30 13:18:38'),(80,1,3,'test',NULL,0,'2016-01-30 13:20:48','2016-01-30 13:20:48'),(81,1,2,'test',NULL,0,'2016-01-30 13:25:02','2016-01-30 13:25:02'),(82,1,1,'test',NULL,0,'2016-01-30 13:26:20','2016-01-30 13:26:20'),(83,1,1,'test',NULL,0,'2016-01-30 13:29:06','2016-01-30 13:29:06'),(84,1,1,'test',NULL,0,'2016-01-30 13:34:20','2016-01-30 13:34:20'),(85,1,1,'test',NULL,0,'2016-01-30 13:37:47','2016-01-30 13:37:47'),(86,1,1,'test',NULL,0,'2016-01-30 13:41:26','2016-01-30 13:41:26'),(87,1,1,'TEST',NULL,0,'2016-01-30 13:42:16','2016-01-30 13:42:16'),(88,1,1,'test',NULL,0,'2016-01-30 13:51:33','2016-01-30 13:51:33'),(89,1,1,'test',NULL,0,'2016-01-30 14:01:57','2016-01-30 14:01:57'),(90,1,1,'test',NULL,0,'2016-01-30 14:06:05','2016-01-30 14:06:05'),(91,1,1,'ddd','apexmd21@gmail.com',0,'2016-01-30 14:17:32','2016-01-30 14:17:57'),(92,1,1,'12wdsadsad','alexpereap@icloud.com',0,'2016-01-30 14:19:12','2016-01-30 14:19:38'),(93,1,1,'121212','test@test.com',0,'2016-01-30 14:20:14','2016-01-30 14:20:35'),(94,1,1,'test','lol@iloud.com',0,'2016-01-30 14:22:00','2016-01-30 14:22:34'),(95,1,1,'test','test@mail.com',0,'2016-01-30 14:23:46','2016-01-30 14:24:11'),(96,1,1,'fdsf','mail@mailer.com',1,'2016-01-30 14:25:16','2016-01-30 14:25:40'),(97,1,1,'test',NULL,0,'2016-01-30 15:12:18','2016-01-30 15:12:18'),(98,1,1,'test',NULL,0,'2016-01-30 15:17:53','2016-01-30 15:17:53'),(99,1,1,'testr','xxx@mail.com',0,'2016-01-30 15:19:32','2016-01-30 15:20:07'),(100,1,1,'tester','tester@mgil.com',0,'2016-01-30 15:32:08','2016-01-30 15:32:37');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_title`
--

DROP TABLE IF EXISTS `users_title`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_title` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) NOT NULL,
  `createdAt` varchar(255) NOT NULL,
  `updatedAt` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `title_UNIQUE` (`title`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_title`
--

LOCK TABLES `users_title` WRITE;
/*!40000 ALTER TABLE `users_title` DISABLE KEYS */;
INSERT INTO `users_title` VALUES (1,'Sr','',NULL),(2,'Sra','',NULL),(3,'Srta','',NULL);
/*!40000 ALTER TABLE `users_title` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `waiter_services`
--

DROP TABLE IF EXISTS `waiter_services`;
/*!50001 DROP VIEW IF EXISTS `waiter_services`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `waiter_services` AS SELECT 
 1 AS `service_id`,
 1 AS `waiter_id`,
 1 AS `service_active`,
 1 AS `service_data`,
 1 AS `service_status_id`,
 1 AS `service_status`,
 1 AS `service_status_message`,
 1 AS `service_status_action`,
 1 AS `table_id`,
 1 AS `table_name`,
 1 AS `table_available`,
 1 AS `user_id`,
 1 AS `restaurant_id`,
 1 AS `user_name`,
 1 AS `user_email`,
 1 AS `user_title_id`,
 1 AS `user_title`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `waiters`
--

DROP TABLE IF EXISTS `waiters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `waiters` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `fk_restaurant_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '1',
  `createdAt` datetime NOT NULL,
  `updatedAt` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username_UNIQUE` (`username`),
  KEY `fk_waiter_restaurants1_idx` (`fk_restaurant_id`),
  CONSTRAINT `fk_waiter_restaurants1` FOREIGN KEY (`fk_restaurant_id`) REFERENCES `restaurants` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `waiters`
--

LOCK TABLES `waiters` WRITE;
/*!40000 ALTER TABLE `waiters` DISABLE KEYS */;
INSERT INTO `waiters` VALUES (1,1,'waiter 1 r1','w1r1','w1r1',1,'0000-00-00 00:00:00',NULL),(2,1,'waiter 2 r1','w2r1','w2r1',1,'0000-00-00 00:00:00',NULL),(3,1,'waiter 3 r1','w3r1','w3r1',1,'0000-00-00 00:00:00',NULL),(4,2,'waiter 2 r1','w1r2','w1r2',1,'0000-00-00 00:00:00',NULL),(5,2,'waiter 2 r2','w2r2','w2r2',1,'0000-00-00 00:00:00',NULL);
/*!40000 ALTER TABLE `waiters` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Final view structure for view `waiter_services`
--

/*!50001 DROP VIEW IF EXISTS `waiter_services`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50001 VIEW `waiter_services` AS select `s`.`id` AS `service_id`,`s`.`fk_waiter_id` AS `waiter_id`,`s`.`active` AS `service_active`,`s`.`data` AS `service_data`,`st`.`id` AS `service_status_id`,`st`.`status` AS `service_status`,`st`.`status_message` AS `service_status_message`,`st`.`action` AS `service_status_action`,`rt`.`id` AS `table_id`,`rt`.`name` AS `table_name`,`rt`.`available` AS `table_available`,`u`.`id` AS `user_id`,`u`.`fk_restaurant_id` AS `restaurant_id`,`u`.`name` AS `user_name`,`u`.`email` AS `user_email`,`ut`.`id` AS `user_title_id`,`ut`.`title` AS `user_title` from ((((`services` `s` join `users` `u` on((`u`.`id` = `s`.`fk_user_id`))) join `users_title` `ut` on((`ut`.`id` = `u`.`fk_user_title_id`))) join `restaurant_tables` `rt` on((`rt`.`id` = `s`.`fk_table_id`))) join `service_status` `st` on((`st`.`id` = `s`.`fk_status_id`))) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-01-30 16:48:40
