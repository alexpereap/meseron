<?php

class Site extends CI_Controller{

     function __construct(){
        parent::__construct();

        if( !$this->session->userdata('admin_data') &&  $this->router->method != 'login' && $this->router->method != 'check_login_credentials' ){

            redirect( site_url('site/login') );
        }

        $this->load->library('grocery_CRUD');
    }

    public function index(){

        // test commit
        $this->load->view("cms/opener");
        $this->load->view("cms/closure");
    }

    public function login(){

        if( $this->session->userdata('admin_data') ){
            redirect( site_url() );
        }

        $this->load->view('cms/login');
    }

    public function check_login_credentials(){

        $result = $this->main->logInCmsUser( $_POST );
        echo json_encode( array('result' => $result) );
    }

    public function restaurants(){

        $this->grocery_crud->set_table('restaurants');
        $this->grocery_crud->unset_fields('createdAt','updatedAt');
        $this->grocery_crud->set_field_upload('logo', 'uploads/restaurant_logos/');
        $this->grocery_crud->callback_before_upload(array($this,'imgValidation'));

        $this->grocery_crud->unset_read();
        $this->grocery_crud->add_action('Editar', 'assets/grocery_crud/themes/flexigrid/css/images/magnifier.png', 'site/restaurantTables');

        $this->grocery_crud->required_fields('username','password','name','logo');
        // $this->grocery_crud->field_type('password', 'password');


        $output = $this->grocery_crud->render();

        $output->in_restaurants = true;
        $output->in_restaurants_list = true;

        $this->load->view("cms/opener", $output);
        $this->load->view("cms/general_output");
        $this->load->view("cms/closure");
    }

     public function imgValidation( $files_to_upload, $field_info ){


        $allowed_extensions = array('jpg', 'jpeg', 'png' );

        foreach( $files_to_upload as $file ){

            $file_ext = end(explode('.', $file['name']) );

            if( !in_array( $file_ext , $allowed_extensions) ){

                return "Solo son permitidos archivos de tipo jpg, swf, png";
            }
        }

        return true;

    }

    public function restaurantTables($restaurantId){
        $this->grocery_crud->set_table('restaurant_tables');
        $this->grocery_crud->where('fk_restaurant_id',$restaurantId);
        $this->grocery_crud->set_relation('fk_restaurant_id','restaurants','name');
        $this->grocery_crud->unset_fields('createdAt','updatedAt');

        $this->grocery_crud->display_as('fk_restaurant_id','Restaurant');
        $this->grocery_crud->callback_before_insert(array($this,'test_callback'));

        $this->grocery_crud->columns('name','fk_restaurant_id');

        $output = $this->grocery_crud->render();

        $output->in_restaurants_tables = true;

        $this->load->view("cms/opener", $output);
        $this->load->view("cms/general_output");
        $this->load->view("cms/closure");
    }

    public function tables(){
        $this->grocery_crud->set_table('restaurant_tables');
        $this->grocery_crud->set_relation('fk_restaurant_id','restaurants','name');
        $this->grocery_crud->unset_fields('createdAt','updatedAt');

        $this->grocery_crud->display_as('fk_restaurant_id','Restaurant');

        $output = $this->grocery_crud->render();

        $output->in_tables = true;

        $this->load->view("cms/opener", $output);
        $this->load->view("cms/general_output");
        $this->load->view("cms/closure");
    }

    public function waiters(){
        $this->grocery_crud->set_table('waiters');
        $this->grocery_crud->set_relation('fk_restaurant_id','restaurants','name');
        $this->grocery_crud->unset_fields('createdAt','updatedAt');

        $this->grocery_crud->display_as('fk_restaurant_id','Restaurant');
        // $this->grocery_crud->field_type('password', 'password');

        $output = $this->grocery_crud->render();

        $output->in_waiters = true;

        $this->load->view("cms/opener", $output);
        $this->load->view("cms/general_output");
        $this->load->view("cms/closure");
    }

    public function logout(){
        // $this->session->sess_destroy();
        $this->session->unset_userdata('admin_data');
        redirect('/');
    }
}