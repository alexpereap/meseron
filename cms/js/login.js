$(document).ready(function(){

    $("#loginForm").submit(function(){

        var form = $(this);

        $.ajax({
            url      : form.attr('action'),
            type     : 'post',
            dataType : 'json',
            data     : form.serialize(),
            success : function(data){

                if( data.result ){
                    // refresh page, internally session has been initiated and the user will be redirected to cms list
                    window.location.reload();
                } else {
                    alert('Nombre de usuario o contraseña incorrectos');
                }
            }
        });

        return false;
    });

});