/**
 * ActionsController
 *
 * @description :: Server-side logic for managing Actions
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

	// recieves a given service id and executes it's action
    'doAction' : function(req,res,next){

        console.log("service received, determining consequent action");

        var service_id = req.param('serviceId');

        VwWaiterServices.findOne({service_id: service_id }).exec(function findOneCB(err, found){

            if( err ) return next(err);

            var vw_waiter_service = found;

            console.log(vw_waiter_service);

            res.json({'message' : 'service received , doing action'});

            var service_status_id = vw_waiter_service.service_status_id;

            if( req.param('triggerOrder') === true ){

                console.log("triggering order status");
                // returns back service to status 3 // take order
                sails.controllers.actions.serviceUpdateStatus( vw_waiter_service, 3 );
                return;

            }

            switch( service_status_id ){

                // status:menu - action :delivered
                case 1:
                    // console.log(sails.controllers  );
                    var newStatusId = 2; // cliente leyendo menú
                    sails.controllers.actions.serviceUpdateStatus( vw_waiter_service, newStatusId );
                    break;

                // status client reading menu, the client order menu
                case 2:
                    var newStatusId = 3; // menu ordered
                    sails.controllers.actions.serviceUpdateStatus( vw_waiter_service, newStatusId );
                    break;

                // status waiter takes order from client
                case 3:
                    var newStatusId = 4 // order delivered
                    sails.controllers.actions.serviceUpdateStatus( vw_waiter_service, newStatusId );
                    break;

                // status waiter delivers order to client
                case 4:
                    var newStatusId = 5; // waiting for another order or the check
                    sails.controllers.actions.serviceUpdateStatus( vw_waiter_service, newStatusId );
                    break;

                // status customer request for paying info
                case 5:
                    var newStatusId = 6; // customer wating for payment info
                    sails.controllers.actions.serviceUpdateStatus( vw_waiter_service, newStatusId );
                    break;

                // status waiter is going to send the check info
                case 6:
                    var newStatusId = 7; // customer picking up his payment method
                    sails.controllers.actions.serviceUpdateStatus( vw_waiter_service, newStatusId, req.param('checkInfo') );
                    break;

                // status customer sends his payment method
                case 7:
                    var newStatusId = 8;
                    sails.controllers.actions.serviceUpdateStatus( vw_waiter_service, newStatusId, req.param('payInfo') );
                    break;

                // waiter mark the service as paid. Interactions ends here
                case 8:
                    sails.controllers.actions.endService( vw_waiter_service );
                    break;

                default:
                    console.log("unknown service status: " + vw_waiter_service.service_status_id);
                    break;

            }
        });
    },

    // status:menu - action :delivered
    'serviceUpdateStatus' : function(waiterService, newStatusId, serviceExtraData){

        var updateData = {
            fk_status_id : newStatusId
        };

        if( typeof serviceExtraData != 'undefined' ){

            updateData.data = JSON.stringify( serviceExtraData );
        }

        console.log("serviceUpdateStatus update data: ");
        console.log(updateData);

        Service.update({id: waiterService.service_id  }, updateData ).exec(function afterwards(err, updated){
            if( err ){
                return err;
            }

            // update services for waiter
            sails.sockets.broadcast( MeseronService.getWrName( waiterService.waiter_id ) , 'updateServices' , 'action done, reload service pool' );
            sails.sockets.broadcast( MeseronService.getTrName( waiterService.table_id ) , 'switchStep' , 'action done, customer switching step' );
        });
    },

    'callWaiter' : function(req,res,next){
        console.log( "calling waiter ..." );

        var service_id = req.param('serviceId');
        VwWaiterServices.findOne({service_id: service_id }).exec(function findOneCB(err, found){
            waiterService = found;

            sendData = {
                'service_id' : waiterService.service_id,
                'user_name'  : waiterService.user_name,
                'user_title' : waiterService.user_title,
                'table_name' : waiterService.table_name
            };

            // executes a waiter call
            sails.sockets.broadcast( MeseronService.getWrName( waiterService.waiter_id ) , 'customerCall' , sendData );

        });
    },
    'endService' : function( waiterService ){

        var updateData = {
            fk_status_id : 9,
            active : 0
        };

        console.log("endService update data: ");
        console.log(updateData);

        console.log( "SERVICE ID : " + waiterService.service_id );

        Service.update({id: waiterService.service_id  }, updateData ).exec(function afterwards(err, updated){
            if( err ){ console.log(err);  }

            Table.update({id: waiterService.table_id }, { available : 1 } ).exec(function afterwards(err, updated){
                console.log("Updated table id " + waiterService.table_id  + " broadcasting messages");

                // update services for waiter
                sails.sockets.broadcast( MeseronService.getWrName( waiterService.waiter_id ) , 'updateServices' , 'action done, reload service pool' );
                sails.sockets.broadcast( MeseronService.getTrName( waiterService.table_id ) , 'switchStep' , 'action done, customer switching step' );
            });

        });
    }
};

