/**
 * UserServiceWaiterController
 *
 * @description :: Server-side logic for managing Userservicewaiters
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var numeral = require('numeral');
module.exports = {

    // control customer redirection according to the service id
    'switchStep' : function(req,res,next){

        var service_id = req.session.Customer.serviceId;

        Service.findOne({id:service_id}).exec(function findOneCB(err, found){

            if( err ) return next(err);

            service = found;

            switch( service.fk_status_id ){

                // client waiting for menu
                case 1:
                    return res.redirect('/customer-step/wait-step');
                    break;

                // client reading menu
                case 2:
                    return res.redirect('/customer-step/ready-to-order');
                    break;

                // waiter taking order
                case 3:
                    return res.redirect('/customer-step/wait-step');
                    break;

                // client waiing for order
                case 4:
                    return res.redirect('/customer-step/wait-step');
                    break;

                // client request check or another order
                case 5:
                    return res.redirect('/customer-step/pay-order')
                    break;

                // client waits for the check info
                case 6:
                    return res.redirect('/customer-step/wait-step');
                    break;

                // client determinates the payment method
                case 7:
                    return res.redirect('/customer-step/payment-method');
                    break;

                // client waiting for waiter to collect the check
                case 8:
                    return res.redirect('/customer-step/wait-step');
                    break;

                // client interaction ended
                case 9:
                    return res.redirect('/user/register');
                    break;

                default:
                    return res.json({ message: 'unknown redirect for service status id: ' + service.fk_status_id });
                    break;

            }
        });

    },

    // general waiting step : alloed service_status_id : 1,3
    'waitStep' : function(req,res,next){

        var allowedServicesStatusId = [1,3,4,6,8];
        var service_id = req.session.Customer.serviceId;

        VwWaiterServices.findOne({service_id:service_id}).exec(function findOneCB(err, found){

            if(err) return next(err);

            // if the service status is not the allowed...
            if( allowedServicesStatusId.indexOf(found.service_status_id) < 0   ){
                return res.redirect( '/customer-step/switchstep' );

            }

            var data = {
                waiter_service:found
            };
            res.view('customer_steps/wait', data);

        });
    },
    // allowed service_status_id : 2
    'readyToOrderStep': function(req,res,next){

        var allowedServicesStatusId = [2];
        var service_id = req.session.Customer.serviceId;

        VwWaiterServices.findOne({service_id:service_id}).exec(function findOneCB(err, found){

            if(err) return next(err);

            // if the service status is not the allowed...
            if( allowedServicesStatusId.indexOf(found.service_status_id) < 0   ){
                return res.redirect( '/customer-step/switchstep' );

            }

            res.view('customer_steps/ready_to_order');

        });
    },

    // customer has the option to make another order or to pay
    'payOrderStep' : function(req,res,next){

        var allowedServicesStatusId = [5];
        var service_id = req.session.Customer.serviceId;

        VwWaiterServices.findOne({service_id:service_id}).exec(function findOneCB(err, found){

            if(err) return next(err);

            // if the service status is not the allowed...
            if( allowedServicesStatusId.indexOf(found.service_status_id) < 0   ){
                return res.redirect( '/customer-step/switchstep' );
            }

            res.view('customer_steps/pay_order');

        });
    },

    // customer decides payment method
    'paymentMethodStep' : function(req,res,next){

        var allowedServicesStatusId = [7];
        var service_id = req.session.Customer.serviceId;

        VwWaiterServices.findOne({service_id:service_id}).exec(function findOneCB(err, found){

            if(err) return next(err);

            // if the service status is not the allowed...
            if( allowedServicesStatusId.indexOf(found.service_status_id) < 0   ){
                return res.redirect( '/customer-step/switchstep' );
            }

            service_data = JSON.parse(found.service_data);

            data = {
                'waiter_service'       : found,
                'service_data'         : service_data,
                'format_check_value'   : numeral(service_data.checkValue).format('0,0'),
                'format_service_value' : numeral(service_data.serviceValue).format('0,0'),
                'format_full_value'    : numeral(service_data.checkValue + service_data.serviceValue).format('0,0'),
            };

            console.log("paymentMethodStep");
            console.log(data);

            res.view('customer_steps/payment_method', data);

        });

    }
};

