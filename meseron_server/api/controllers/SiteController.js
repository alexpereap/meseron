/**
 * SiteController
 *
 * @description :: Server-side logic for managing Sites
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

    'comeBackSoon' : function(req,res,next){
        return res.view('site/comeback');
    }
};

