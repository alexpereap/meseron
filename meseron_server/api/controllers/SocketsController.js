/**
 * SocketsController
 *
 * @description :: Server-side logic for managing Sockets
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

    // Gneral restaurant room
    'subscribeRestaurantRoom': function(req,res,next){

        console.log("Joining to Restaruant room");

        var restaurantId = req.param('restaurantId');
        var roomName = MeseronService.getRrName( req.param('restaurantId') );

        sails.sockets.join(req.socket, roomName);
        res.json({
            message: 'Subscribed to a restaurant room called "'+roomName+'" :)'
        });
    },
    // General Table Room
    'subscribeTableRoom' : function (req,res,next){


        console.log("Joining to table room");

        var tableId = req.param('tableId');
        var roomName = MeseronService.getTrName( tableId );

        console.log("ROOM NAME: ");
        console.log(roomName);

        sails.sockets.join(req.socket, roomName);
        res.json({
            message: 'Subscribed to the room: "'+roomName+'" :)'
        });

    },
    // Personal Waiter Room
    'subscribeToWaiterRoom' : function(req,res,next){
        console.log("Joining to Waiter room");

        var waiterId = req.param('waiterId');
        var roomName = MeseronService.getWrName( waiterId );

        console.log("ROOM NAME: ");
        console.log(roomName);

        sails.sockets.join(req.socket, roomName);
        res.json({
            message: 'Subscribed to the room: "'+roomName+'" :)'
        });
    }
};

