/**
 * TableController
 *
 * @description :: Server-side logic for managing Tables
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

    // handles table register - or - auth
	'index': function(req,res,next){

		Table.findOne(req.param('id'), function foundTable(err, table){
            if (err) return next(err);

            // Checks table availability
            if( table.available == 0 ){
                return res.json( {'status': 'This table is not available' } );

                // TODO: redirection to current servie status page
            }

            UserTitle.find( function foundUsers(err,user_titles){

                if (err) return next(err);
                if(!table) return next();

                var data = {
                    'restaurantId'   : table.fk_restaurant_id,
                    'restaurantRoom' : MeseronService.getRrName( table.fk_restaurant_id ),
                    'userTitles'     : user_titles,
                    'tableId'        : req.param('id')
                };

                console.log(data);

                // console.log(data);
                var customerRestaurantRoom = data.restaurantRoom;

                transmitionData = {
                    'table'     : table,
                    'tableRoom' : MeseronService.getTrName( req.param('id') )
                }

                console.log("customer broadcast to " + customerRestaurantRoom);
                sails.sockets.broadcast( customerRestaurantRoom , 'newCustomer' ,  transmitionData );

                res.view('client/index', data);

            });

        });

        // res.view('client/index');
    },
};

