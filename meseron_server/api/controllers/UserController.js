/**
 * UserController
 *
 * @description :: Server-side logic for managing Users
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

	'create' : function(req,res,next){

        if( typeof req.param('name') == 'undefined'  || req.param('name') == '' ){

            var nameRequiredError = [{name: 'nameRequiredError', message:'Por favor especifica tu nombre'}];

            req.session.flash = {
                err: nameRequiredError
            };

            return res.redirect('back');
        }

        data = {
            name : req.param('name'),
            fk_title_id : req.param('title')
        }

        User.create( data ).exec(function createCB(err, created){

            if( err ) next(err);
            console.log('Created user with id ' + created.id);
        });
    },

    'login' : function(req,res,next){

    },

    'beginInteractionWithWaiter' : function(req,res,next){


        if( req.param('email') != '' ){
            User.findOne( {email: req.param('email') } ).exec(function findOneCB(err, user){

                if( typeof user != 'undefined' ){
                    loginUser(user);
                } else {

                    var emailNotFound = [{name: 'emailNotFound', message:'No pudimos encontrar el email: ' + req.param('email') }];

                    req.session.flash = {
                        err: emailNotFound
                    };

                    return res.redirect('back');
                }

            });
        } else {
            createUser();
        }

        function loginUser(user){
            var serviceData = {
                'fk_waiter_id'  : req.param('waiterId'),
                'fk_table_id'   : req.param('tableId'),
                'fk_user_id'    : user.id,
                'fk_status_id'  : 1, // status "menu"
            };

            UserTitle.findOne( user.fk_user_title_id ).exec(function findOneCB(err, userTitle){
                if( err ) return next(err);

                Service.create( serviceData ).exec(function createCB(err, created){

                        if(err) return next(err);

                        console.log('Created service ' + created.id);
                        var service = created;

                        // Log user in
                        req.session.authenticated = true;
                        customerData = {
                            id           : user.id,
                            name         : req.param('name'),
                            title_id     : req.param('title'),
                            title        : userTitle.title,
                            restaurantId : req.param('restaurantId'),
                            tableId      : req.param('tableId'),
                            waiterRoom   : MeseronService.getWrName( req.param('waiterId') ),
                            tableRoom    : MeseronService.getTrName( req.param('tableId') ),
                            waiterId     : req.param('waiterId'),
                            serviceId    : service.id,
                            registered   : true
                        };

                        req.session.Customer = customerData;

                        console.log( 'broadcast new service to ' + MeseronService.getWrName( req.param('waiterId') ) );
                        sails.sockets.broadcast( MeseronService.getWrName( req.param('waiterId') ) , 'updateServices' , 'new service get, reload service pool' );

                        Table.update({id: req.param('tableId') }, { available: 0 } ).exec(function afterwards(err, updated){

                            if( err ) return next(err);
                            console.log('Updated table ID ' + updated[0].id);

                            return res.redirect('/customer-step/wait-step');
                        });

                    });
            });
        }

        function createUser(){


            if( typeof req.param('name') == 'undefined'  || req.param('name') == '' ){

                var nameRequiredError = [{name: 'nameRequiredError', message:'Por favor especifica tu nombre'}];

                req.session.flash = {
                    err: nameRequiredError
                };

                return res.redirect('back');
            }

            UserTitle.findOne( req.param('title') ).exec(function findOneCB(err, userTitle){

                if(err) return next(err);

                customerRestaurantRoom = MeseronService.getWrName( req.param('waiterId') );

                // Register User in DB
                var userData = {
                    fk_restaurant_id : req.param('restaurantId'),
                    fk_user_title_id : req.param('title'),
                    name             : req.param('name')
                };

                User.create( userData ).exec(function createCB(err,created){
                    if(err) return next(err);
                    console.log("user created id: " + created.id );

                    var user = created;

                    // Stores the service

                    var serviceData = {
                        'fk_waiter_id'  : req.param('waiterId'),
                        'fk_table_id'   : req.param('tableId'),
                        'fk_user_id'    : user.id,
                        'fk_status_id'  : 1, // status "menu"
                    };

                    console.log(serviceData);

                    Service.create( serviceData ).exec(function createCB(err, created){

                        if(err) return next(err);

                        console.log('Created service ' + created.id);
                        var service = created;

                        // Log user in
                        req.session.authenticated = true;
                        customerData = {
                            id           : user.id,
                            name         : req.param('name'),
                            title_id     : req.param('title'),
                            title        : userTitle.title,
                            restaurantId : req.param('restaurantId'),
                            tableId      : req.param('tableId'),
                            waiterRoom   : MeseronService.getWrName( req.param('waiterId') ),
                            tableRoom    : MeseronService.getTrName( req.param('tableId') ),
                            waiterId     : req.param('waiterId'),
                            serviceId    : service.id,
                            registered   : false
                        };

                        req.session.Customer = customerData;

                        console.log( 'broadcast new service to ' + MeseronService.getWrName( req.param('waiterId') ) );
                        sails.sockets.broadcast( MeseronService.getWrName( req.param('waiterId') ) , 'updateServices' , 'new service get, reload service pool' );

                        Table.update({id: req.param('tableId') }, { available: 0 } ).exec(function afterwards(err, updated){

                            if( err ) return next(err);
                            console.log('Updated table ID ' + updated[0].id);

                            return res.redirect('/customer-step/wait-step');
                        });

                    });
                });

                // broadcasts to waiters subscribed to restaurant
                // sails.sockets.broadcast( customerRestaurantRoom , 'newCustomer' , customerData );
            });
        }



    },
    'postlogout' : function(req,res,next){

        console.log("ending session");
        req.session.destroy();

        return res.json( {message: 'ok session terminated'} );
    },

    // register user at the end of interaction
    'register':function(req,res,next){

        if( typeof req.session.Customer == 'undefined'  || req.session.Customer.registered == true ){

            console.log("customer already registered or no session ... redirecting")
            return res.redirect('/comebacksoon');
        }

        if( req.session.authenticated == true ){
            req.session.authenticated = false;
        }

        return res.view('client/register');

    },

    'updateRegister':function(req,res,next){

        User.findOne( {email: req.param('email') } ).exec(function findOneCB(err, user){

            console.log("USER: ");
            console.log(user);

            if( typeof user != 'undefined' ){
                var emailExistsError = [{name: 'emailExistsError', message:'El email' + user.email + ' ya se encuentra registrado.'  }];

                req.session.flash = {
                    err: emailExistsError
                };

                return res.redirect('back');
            }

            console.log("UPDATING: " + req.session.Customer.id);

            updateData = {
                email : req.param('email')
            };

            console.log(typeof req.param('subscribed'));
            console.log( req.param('subscribed') );

            if( typeof req.param('subscribed') != 'undefined' ){

                console.log("WTF");
                updateData.subscribed = 1;
            };

            console.log(updateData);

            User.update({id: req.session.Customer.id  }, updateData ).exec(function afterwards(err, updated){
                if( err ) return next(err);

                req.session.destroy();
                return res.redirect('/comebacksoon');

            });

        });
    }
};

