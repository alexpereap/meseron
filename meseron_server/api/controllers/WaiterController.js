/**
 * WaiterController
 *
 * @description :: Server-side logic for managing Waiters
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

    'dashboard': function(req,res,next){

        var Flow = {};

        console.log({ waiter_id : req.session.Waiter.id });
        VwWaiterServices.find( { waiter_id : req.session.Waiter.id, service_active : 1  } ).exec(function findOneCB(err, waiterServices){

            if( err ) return next(err);
            Flow.waiterServicesFound(waiterServices);
        });

        Flow.waiterServicesFound =  function (waiterServices){

            data = {
                'restaurant_id'  : req.session.Waiter.fk_restaurant_id,
                'waiterServices' : waiterServices
            };

            console.log(data);

            return res.view('waiter/dashboard', data);
        }



    },
    // take service from customer
    'takeService' : function(req,res,next){

        var reqData = req.params.all();

        transmitionData = {
            waiter     : req.session.Waiter,
            waiterRoom : MeseronService.getWrName( req.session.Waiter.id )
        }

        console.log("broadcast to " + reqData.customer_info.tableRoom);
        sails.sockets.broadcast( reqData.customer_info.tableRoom , 'serviceTaken' ,  transmitionData );

    },
    // broadcasts to restaurant room that certain service was taken
    'serviceTaken' : function(req,res,next){
        var reqData = req.params.all();

        transmitionData = {
            table_id : reqData.tableId
        };

        console.log(transmitionData);

        restaurantRoom = MeseronService.getRrName( req.session.Waiter.fk_restaurant_id );

        console.log("broadcast to " + restaurantRoom);
        sails.sockets.broadcast( restaurantRoom , 'serviceTaken' ,  transmitionData );
    },

    // get the authenticated waiter services
    'getMyServices' : function(req,res,next){

        VwWaiterServices.find( { waiter_id : req.session.Waiter.id, service_active : 1  } ).exec(function findOneCB(err, waiterServices){

            if( err ) return next(err);

            return res.json( waiterServices );

        });
    },

    'endService': function(req,res,next){

        Table.update({id: req.param('table_id') }, { available : 1 } ).exec(function afterwards(err, updated){
            if (err) return next(err);

            console.log('Updated table ID ' + updated[0].id);

            Service.update({ id: req.param('service_id')  }, { active : 0 } ).exec(function afterwards(err, updated){
                console.log( "Service updated ID " + updated[0].id );

                tableRoom = MeseronService.getTrName( req.param('table_id') );
                console.log("broadcast to " + tableRoom );
                sails.sockets.broadcast( tableRoom , 'endService' , 'terminate service now' );

            });
        });
    }
};

