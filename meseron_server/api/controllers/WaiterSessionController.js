/**
 * WaiterSessionController
 *
 * @description :: Server-side logic for managing Waitersessions
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {

    'login': function(req,res,next){


        if( req.session.authenticated ){

            res.redirect('/waiter/dashboard');
            return;
        }

        res.view('waiter/login');
    },

	'logon': function(req,res,next){

        // check filled fields
        if( !req.param('username') || !req.param('password') ){
            var usernamePasswordRequiredError = [{name: 'usernamePasswordRequred', message:'You must enter both a username and password'}];

            req.session.flash = {
                err: usernamePasswordRequiredError
            };

            res.redirect('/waiter/login');
            return;
        }

        Waiter.findOne({username: req.param('username') }).exec(function findOneCB(err, waiter){

            if(err) return next(err);

            // check if email exists
            if(!waiter){
                var noAccountError = [{ name: 'noAccount', message: 'The username ' + req.param('username') + ' not found.' }];
                req.session.flash = {
                    err: noAccountError
                };

                res.redirect('/waiter/login');
                return;
            }

            // compare password and username
            if( req.param('password') != waiter.password ){

            	var usernamePasswordMissMatcherror = [{ name: 'usernamePasswordMissMatch', message: 'Invalid username and password combination.' }];
            	req.session.flash = {
            		err: usernamePasswordMissMatcherror
            	};

            	res.redirect('/waiter/login');
            	return;
            }

            // Log user in
            req.session.authenticated = true;
            req.session.Waiter = waiter;

            res.redirect('waiter/dashboard');
            return;

        });
    },

    'logout': function(req,res,next){

        Waiter.findOne({id: req.session.Waiter.id }).exec(function findOneCB(err, waiter){

            if(err) return next(err);

            var waiterId = req.session.Waiter.id;
            // wipe out the session
            req.session.destroy();

            // redirect browser to the sign in screen
            res.redirect('/waiter/login');

        });
    }
};

