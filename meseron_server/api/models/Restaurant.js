/**
* Restaurant.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

	tableName: 'restaurants',
	attributes: {
        name: {
            type: 'string',
            required: true
        },
        logo: {
            type: 'string',
            required: true
        }
    }
};

