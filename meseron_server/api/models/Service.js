/**
* Service.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

	tableName: 'services',
  	attributes: {

  		fk_waiter_id: {
            type: 'integer',
            required: true
        },
        fk_user_id: {
            type: 'integer',
            required: true
        },
        fk_table_id: {
            type: 'integer',
            required: true
        },
        fk_status_id : {
            type : 'integer',
            required : true
        },
        active: {
            type: 'boolean',
            required: true,
            defaultsTo : true
        },
        data: {
            type: 'string',
            required: false
        }
  	}
};

