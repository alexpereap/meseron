/**
* ServiceStatus.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

    tableName: 'service_status',
    attributes: {

        status: {
            type: 'string',
            required: true
        },
        action: {
            type: 'string',
            required: true
        }

    }
};

