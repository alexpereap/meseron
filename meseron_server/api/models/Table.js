/**
* Table.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

    tableName: 'restaurant_tables',
    attributes: {
        fk_restaurant_id: {
            type: 'integer',
            required: true
        },

        name: {
            type: 'string',
            required: true
        },

        available:{
            type:'boolean',
            required: true,
            defaultsTo : true
        }
    }
};

