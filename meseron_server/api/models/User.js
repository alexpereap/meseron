/**
* User.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

	tableName: 'users',
  	attributes: {
  		fk_restaurant_id: {
            type: 'integer',
            required: true
        },
        fk_user_title_id: {
            type: 'integer',
            required: true
        },
        name: {
            type: 'string',
            required: true
        },
        email: {
            type: 'string',
            required: false
        },
        subscribed: {
            type: 'boolean',
            required: false,
            defaultsTo: false
        },
  	}
};

