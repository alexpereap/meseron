/**
* VwWaiterServices.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

    tableName: 'waiter_services',
    attributes: {
        service_id: {
            type: 'integer',
        },
        waiter_id: {
            type: 'integer',
        },
        service_active: {
            type: 'boolean',
        },
        service_data: {
            type: 'string',
        },
        service_status_id: {
            type: 'integer',
        },
        service_status: {
            type: 'string',
        },
        service_status_message:{
            type:'string'
        },
        service_status_action: {
            type: 'string',
        },
        table_id: {
            type: 'integer',
        },
        table_name: {
            type: 'string',
        },
        table_available: {
            type: 'boolean',
        },
        user_id: {
            type: 'integer',
        },
        restaurant_id: {
            type: 'integer',
        },
        user_name: {
            type: 'string',
        },
        user_email: {
            type: 'string',
        },
        user_title_id: {
            type: 'integer',
        },
        user_title: {
            type: 'string',
        },
    },
    autoPK:false,
    autoCreatedAt: false,
    autoUpdatedAt: false
};

