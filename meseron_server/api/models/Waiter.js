/**
* Waiter.js
*
* @description :: TODO: You might write a short summary of how this model works and what it represents here.
* @docs        :: http://sailsjs.org/#!documentation/models
*/

module.exports = {

	tableName: 'waiters',
    attributes: {

        fk_restaurant_id: {
            type: 'integer',
            required: true
        },
        name: {
            type: 'string',
            required: true
        },
        username: {
            type: 'string',
            required: true
        },
        password: {
            type: 'integer',
            required: true
        },
        active: {
            type: 'boolean',
            required: true,
            defaultsTo:true
        },
    }
};

