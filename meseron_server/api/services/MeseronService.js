// Meseron Services

module.exports = {

    // returns restaurant room name for socket
    getRrName: function(restaurantId) {

        return 'restaurant_id_' + restaurantId + '_room';

    },

    // returns table room name for socket
    getTrName: function(tableId) {

        return 'table_id_' + tableId + '_room';

    },

    // returns waiter room name for socket
    getWrName: function(waiterId) {

        return 'waiter_id_' + waiterId + '_room';

    },

    updateRestaurantTable: function (tableId, data){

        console.log("updating table active status");

        Table.update({id: tableId }, data ).exec(function afterwards(err, updated){

          if (err) {
            // handle error here- e.g. `res.serverError(err);`
            return;
          }

          console.log('Updated table ID ' + updated[0].id);
        });
    }
};