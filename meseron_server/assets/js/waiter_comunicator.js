// Prevent back forward cache
$(window).bind("pageshow", function(event) {
    if (event.originalEvent.persisted) {
        window.location.reload();
    }
});

// Angular APP
var waiterApp = angular.module('waiterApp',[]);
var csrf;
var newCustomerData = {};


window.onload = function init(){

    console.log("app READY");


        io.socket.get('/csrfToken', function (resData){
            csrf = resData._csrf;
        });

        // General Waiter part on sockets
        if( typeof $("#iAmWaiter").val() != 'undefined' ){

            if( typeof $("#restaurantId").val() != "undefined" ){

                var restaurantId = $("#restaurantId").val();
                var waiterId = $("#waiterId").val();

                // subscribes to designited restaurant room
                io.socket.get('/subscribe-restaurant-room/' + restaurantId, function(resData){
                    console.log(resData);
                });

                // subscribes to personal waiter room
                io.socket.get('/subscribe-waiter-room/' + waiterId, function(resData){
                    console.log(resData);
                });
            }
        }

        // -------------------------------------------------------------------------------------------------------------------------------------------------- //

        // General Customer part on sockets
        if( typeof $("#iAmCustomer").val() != 'undefined' ){
            console.log("you're a customer");


            var tableId = $("#tableId").val();

            // subscribes to table room
            io.socket.get('/subscribe-table-room/' + tableId, function(resData){
                console.log(resData);
            });

            // when service over redirects to home
            io.socket.on('endService', function onServerSentEvent (msg) {
                console.log(msg);

                io.socket.post('/user/postlogout', { _csrf : csrf }, function (resData){

                    console.log(resData);
                    console.log("ok redirectme now");
                    window.location.replace('/');
                });
            });

            // switch step when waiter sends request for it
            io.socket.on('switchStep', function onServerSentEvent(msg){

                console.log(msg);
                window.location.replace( '/customer-step/switchstep' );

            });

            if( typeof $("#subscribeToTableRoom").val() != 'undefined' ){

                // triggers scan modal
                $("#customerScanModal").modal('show');

                // listen for service taken for waiter
                io.socket.on('serviceTaken', function onServerSentEvent (msg) {

                    console.log(msg);

                    $("#msgWaiterName").text( msg.waiter.name );
                    $("#waiterId").val( msg.waiter.id );
                    $("#customerScanModal").modal('hide');
                });
            }


        }

    io.socket.on('disconnect', function(){
      console.log('Lost connection to server');
    });

}; // load end

waiterApp.service('paymentInfo', function() {

    var self = this;
    this.serviceId = null;

    this.setServiceId = function(data) {

        console.log("setting service id");
        console.log(data);

        self.serviceId = data;
        return self.serviceId;
    };
});

    // Waiter pending services list
    waiterApp.controller('incomingServices', function($scope){

        $scope.incomingServices = [];

            // listen for new customers scaning the table qr code
            io.socket.on('newCustomer', function onServerSentEvent (msg) {

                console.log(msg);
                newCustomerData = msg;
                console.log("new customer arrived, filling list");

                repeated_service = false;

                // checks is not repeated service
                for( x in $scope.incomingServices ){

                    if( $scope.incomingServices[x].table.id == msg.table.id ){
                        repeated_service = true;
                        break;
                    }
                }

                if( repeated_service === false ){

                    $scope.incomingServices.push( msg );
                }

                $scope.$digest(); // update the scope -_-
                console.log($scope.incomingServices);

                $("#msgTableName").text( msg.table.name );
            });

            // Removes a table from the list when a waiter takes the service
            io.socket.on('serviceTaken', function onServerSentEvent (msg) {

                table_id = msg.table_id;

                console.log("removing service from table id : " + table_id );

                for( x in $scope.incomingServices ){

                    if( $scope.incomingServices[x].table.id == table_id ){

                        console.log("removed index " + x);
                        $scope.incomingServices.splice(x, 1);
                        $scope.$digest();
                        break;
                    }
                }
            });

        io.socket.on('customerCall', function onServerSentEvent (msg) {

            console.log("customer called!");
            console.log(msg);

            $("#customerCall").modal('show');
            $("#customerMessage").text( "El " + msg.user_title + ' ' + msg.user_name + ' Solicita tu atención en la mesa ' + msg.table_name );

        });


        $scope.ignoreCustomer = function(index){

            // removes item from list
            $scope.incomingServices.splice(index, 1);
        }

        $scope.takeService = function(index){

            customerInfo = $scope.incomingServices[index];

            // removes item from list
            $scope.incomingServices.splice(index, 1);

            sendData = {
                customer_info : customerInfo,
                _csrf : csrf
            };

            console.log(sendData);

            // sends message to table room to start interaction
            io.socket.post('/waiter/take-service', sendData, function (resData){
                console.log(resData);
            });

            // sends message to restaurant N room to remove service request from other waiters
            sendData = {
                tableId : customerInfo.table.id,
                _csrf : csrf
            };

            io.socket.post('/waiter/service-taken', sendData, function (resData){
                console.log(resData);
            });
        }

    });

// waiter services on dashboard
waiterApp.controller('waiterServices', function($scope, paymentInfo){

        $scope.waiterCustomers =[];

        io.socket.get('/waiter/get-my-services', function (resData){

            resData = parseWaiterCustomers(resData);

            $scope.waiterCustomers = resData;
            $scope.$digest();
        });


        // update waiter customers trough socket
        io.socket.on('updateServices', function onServerSentEvent (msg) {

            io.socket.get('/waiter/get-my-services', function (resData){

                resData = parseWaiterCustomers(resData);

                $scope.waiterCustomers = resData;
                $scope.$digest();
            });
        });

    $scope.endService = function(index){


        // stores service info in variable
        service = $scope.waiterCustomers[index];

        // removes the service from the list
        $scope.waiterCustomers.splice(index, 1);

        console.log(service);

        sendData = {
            table_id   : service.table_id,
            service_id : service.service_id,
            _csrf : csrf
        };


        io.socket.post('/waiter/endService', sendData, function (resData){
            console.log(resData);
        });

    };

    $scope.doServiceAction = function( serviceId, serviceStatusId ){

        console.log("do Service Action");
        console.log("service id " + serviceId);
        console.log("service statud id" + serviceStatusId);

        // waiter sending the check info
        if( serviceStatusId == 6 ){

            // service Id updated in service to be accesed by the modalbox
            paymentInfo.setServiceId(serviceId);

            $("#paymentModal").modal('show');
            return false;

        }

        sendData = {
            serviceId : serviceId,
            _csrf : csrf
        };

        io.socket.post('/waiter-customer/do-action/', sendData, function (resData){

            console.log(resData);

        });
    };

    function parseWaiterCustomers( waiterCustomer ){
        // controls payment info

        try{
            for( x in waiterCustomer ){

                console.log( waiterCustomer[x].service_status_id );

                if( waiterCustomer[x].service_status_id == 8 ){

                    service_data = JSON.parse( waiterCustomer[x].service_data );
                    console.log(service_data);

                    switch( service_data.payType ){

                        case 'cash':
                        status_msg = "Cliente pagará su cuenta con efectivo, ha decidido pagar de servicio: $" + service_data.serviceValue;
                        break;

                        case 'dataphone':
                        status_msg = "Cliente pagará su cuenta con tarjeta debito / credito, ha decidido pagar de servicio: $" + service_data.serviceValue;
                        break;

                    }

                    waiterCustomer[x].service_status = status_msg;

                }
            }
        return waiterCustomer;
        }
        catch(err){
            console.log("Frontend Javascript Exception ocurred, error: " + err);
        }

    }

});

waiterApp.controller('paymentModal', function($scope, paymentInfo){

    $scope.checkValue;
    $scope.serviceValue;

    $scope.sendCheck = function(){

        serviceId = paymentInfo.serviceId;

        console.log( "check: " +  $scope.checkValue );
        console.log( "service: " + $scope.serviceValue );
        console.log( "serviceID: " + serviceId);

        if( isNaN( $scope.checkValue ) || isNaN( $scope.serviceValue ) ){
            alert("Inserta valores validos");
            return false;
        }

        if( $scope.checkValue < 0 || $scope.serviceValue < 0 ){
            alert("Inserta valores positivos");
            return false;
        }

        sendData = {
            serviceId    : serviceId,
            checkInfo    :{
                checkValue   : $scope.checkValue,
                serviceValue : $scope.serviceValue,
            },
            _csrf : csrf
        };

        console.log(sendData);

        io.socket.post('/waiter-customer/do-action/', sendData, function (resData){

            console.log(resData);
            $("#paymentModal").modal('hide');

        });
    }

});

waiterApp.controller('readyToOrder', function($scope){
    console.log("ready to order step");

    var tableId = $("#tableId").val();
    var serviceId = $("#serviceId").val();

    $scope.orderDone = false;

   $scope.orderNow = function(){

        $scope.orderDone = true;
        console.log("service id: " +  serviceId);
        console.log("submiting order request to waiter");

        sendData = {
            serviceId : serviceId,
            _csrf : csrf
        }

        io.socket.post('/waiter-customer/do-action/', sendData, function (resData){

            console.log(resData);

        });
   }
});

waiterApp.controller('payOrOrder', function($scope){

    console.log("ready to pay or order");
    var tableId = $("#tableId").val();
    var serviceId = $("#serviceId").val();

    $scope.actionDone = false;

    $scope.orderNow = function(){

        sendData = {
            serviceId    : serviceId,
            triggerOrder : true, // rewinds to take order flow
            _csrf : csrf
        };

        io.socket.post('/waiter-customer/do-action/', sendData, function (resData){

            console.log(resData);

        });
    }

    $scope.payNow = function(){

        $scope.actionDone = true;

        sendData = {
            serviceId    : serviceId,
            _csrf : csrf
        };

        io.socket.post('/waiter-customer/do-action/', sendData, function (resData){

            console.log(resData);

        });

    }

});

waiterApp.controller('paymentMethod', function($scope){

    var serviceId = $("#serviceId").val();
    $scope.payType;
    $scope.serviceValue;

    $scope.orderNow = function(){

        sendData = {
            serviceId    : serviceId,
            triggerOrder : true, // rewinds to take order flow
            _csrf : csrf
        };

        io.socket.post('/waiter-customer/do-action/', sendData, function (resData){

            console.log(resData);

        });
    }

    $scope.callWaiter = function(){
        sendData = {
            serviceId    : serviceId,
            _csrf : csrf
        };

        io.socket.post('/waiter-customer/call-waiter/', sendData, function (resData){

            console.log(resData);

        });
    }

    $scope.setPaymentMethod = function(payType){

        $scope.payType = payType;
        $("#payData").modal('show');
    }

    $scope.sendPaymentInfo = function(){

        if( isNaN( $scope.serviceValue ) ){
            alert("Ingresa un valor valido");
            return;
        }

        if( $scope.serviceValue < 0 ){
            alert("El servicio no puede ser menor a 0");
            return;
        }

        sendData = {
            serviceId    : serviceId,
            payInfo : {
                payType      : $scope.payType,
                serviceValue : $scope.serviceValue
            },
            _csrf : csrf
        };

        console.log(sendData);

        io.socket.post('/waiter-customer/do-action/', sendData, function (resData){

            console.log(resData);

        });
    }


});
