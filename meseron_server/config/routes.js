/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes map URLs to views and controllers.
 *
 * If Sails receives a URL that doesn't match any of the routes below,
 * it will check for matching files (images, scripts, stylesheets, etc.)
 * in your assets directory.  e.g. `http://localhost:1337/images/foo.jpg`
 * might match an image file: `/assets/images/foo.jpg`
 *
 * Finally, if those don't match either, the default 404 handler is triggered.
 * See `api/responses/notFound.js` to adjust your app's 404 logic.
 *
 * Note: Sails doesn't ACTUALLY serve stuff from `assets`-- the default Gruntfile in Sails copies
 * flat files from `assets` to `.tmp/public`.  This allows you to do things like compile LESS or
 * CoffeeScript for the front-end.
 *
 * For more information on configuring custom routes, check out:
 * http://sailsjs.org/#!/documentation/concepts/Routes/RouteTargetSyntax.html
 */

module.exports.routes = {

  /***************************************************************************
  *                                                                          *
  * Make the view located at `views/homepage.ejs` (or `views/homepage.jade`, *
  * etc. depending on your default view engine) your home page.              *
  *                                                                          *
  * (Alternatively, remove this and add an `index.html` file in your         *
  * `assets` directory)                                                      *
  *                                                                          *
  ***************************************************************************/

  'get /': {
    view: 'homepage'
  },

  'get /comebacksoon' : {
    controller : 'SiteController',
    action: 'comeBackSoon'
  },

  // TABLE ROUTES

  'get /table/:id': {
    controller: 'TableController',
    action: 'index'
  },


  // WAITER ROUTES

  'get /waiter/login': {
    controller: 'WaiterSessionController',
    action: 'login'
  },

  'post /waiter/logon':{
      controller: 'WaiterSessionController',
      action: 'logon'
  },

  'get /waiter/dashboard':{
      controller: 'WaiterController',
      action: 'dashboard'
  },

  'get /waiter/logout':{
      controller: 'WaiterSessionController',
      action: 'logout'
  },

  'get /waiter/get-my-services' :{
      controller: 'WaiterController',
      action: 'getMyServices'
  },

  'post /waiter/endService' :{
      controller: 'WaiterController',
      action: 'endService'
  },

  // customer steps service routes
  'get /customer-step/ready-to-order' :{
      controller: 'CustomerSteps',
      action: 'readyToOrderStep'
  },

  'get /customer-step/wait-step' : {
      controller: 'CustomerSteps',
      action: 'waitStep'
  },

  'get /customer-step/switchstep':{
      controller: 'CustomerSteps',
      action: 'switchStep'
  },

  'get /customer-step/pay-order' : {
      controller: 'CustomerSteps',
      action: 'payOrderStep'
  },
  'get /customer-step/payment-method' : {
      controller: 'CustomerSteps',
      action: 'paymentMethodStep'
  },

  // Services Actions Routes

  'post /waiter-customer/do-action':{
    controller:'ActionsController',
    action: 'doAction'
  },

  'post /waiter-customer/call-waiter/' : {
    controller: 'ActionsController',
    action: 'callWaiter'
  },

  // USER ROUTES

  'post /user/login':{
      controller: 'UserController',
      action: 'login'
  },

  'post /user/create':{
      controller: 'UserController',
      action: 'create'
  },

  'post /user/begin-interaction' : {
      controller : 'UserController',
      action     : 'beginInteractionWithWaiter'
  },

  'post /user/postlogout' : {
      controller : 'UserController',
      action     : 'postlogout'
  },

  'get /user/register' : {
      controller: 'UserController',
      action : 'register'
  },

  'post /user/updateregister' : {
      controller : 'UserController',
      action     : 'updateRegister',
  },

  // SOCKET ROUTES
  '/subscribe-restaurant-room/:restaurantId':{
    controller: 'SocketsController',
    action: 'subscribeRestaurantRoom'
  },

  '/subscribe-waiter-room/:waiterId':{
    controller:'SocketsController',
    action: 'subscribeToWaiterRoom'
  },

  // SOCKET ROUTES
  '/subscribe-table-room/:tableId':{
    controller: 'SocketsController',
    action: 'subscribeTableRoom'
  },
  'post /waiter/take-service':{
      controller: 'WaiterController',
      action: 'takeService'
  },
  'post /waiter/service-taken':{
      controller: 'WaiterController',
      action: 'serviceTaken'
  }

  /***************************************************************************
  *                                                                          *
  * Custom routes here...                                                    *
  *                                                                          *
  * If a request to a URL doesn't match any of the custom routes above, it   *
  * is matched against Sails route blueprints. See `config/blueprints.js`    *
  * for configuration options and examples.                                  *
  *                                                                          *
  ***************************************************************************/

};
